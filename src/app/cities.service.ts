import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  URL:string = 'https://hosc4bpf20.execute-api.us-east-1.amazonaws.com/default'; 
    constructor(private http:HttpClient) { }

    
  
    predictCity(temperature,humidity){
      const body = {
        data:{
          temperature:temperature,
          humidity:humidity
        
      }
    
    }
    return this.http.post<any>(this.URL,body).pipe(
      map(res => {
        const final:string = res.body;
        console.log({final});
        return final;
      })
    )
    }}
