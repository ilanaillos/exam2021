import { Component, OnInit } from '@angular/core';
import { CitiesService } from './../cities.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Weather } from './../interfaces/weather';
import { WeatherService } from './../weather.service';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {
  cities:Object[] = [{id:1, name:'London',temperature:'25',humidity:'67'},{id:2, name:'Paris',temperature:'18',humidity:'45'},{id:3, name:'Tel Aviv',temperature:'31',humidity:'14'},{id:4, name:'Jerusalem',temperature:'23',humidity:'83'},{id:5, name:'Berlin',temperature:'10',humidity:'15'},{id:6, name:'Rome',temperature:'12',humidity:'67'},{id:7, name:'Dubai',temperature:'29',humidity:'50'},{id:8, name:'Athens',temperature:'17',humidity:'30'}];
  city:string;


displayedColumns: string[] = ['name', 'getData', 'temperature','humidity','predict']; 
image:string; 
lon:number; 
lat:number;
temperature:number;
humidity:number;

predictResult:string;
selectedValue:boolean = false;
result$:Observable<string>;
country:string; 
weatherData$:Observable<Weather>;
hasError:Boolean = false;
errorMessage:string;
citiesService: any;
 predictsOptions = [{value:false,viewValue:'Wil rain'},{value:true,viewValue:'Will not rain'}];

constructor(private route:ActivatedRoute, private weatherService:WeatherService) {}


// predict(){
//   this.result$ = this.citiesService.predictStudent(this.temperature,this.humidity);
//   this.result$.subscribe(
//     res => {
//       this.predictResult = res;
//       if(Number(res) > 0.5){
//         this.selectedValue = false;
//       }else{
//         this.selectedValue = true;
//       }
//     }
//   )
// }

ngOnInit(): void {
 this.city = this.route.snapshot.params.city; 
 this.weatherData$ = this.weatherService.searchWeatherData(this.city); 
 this.weatherData$.subscribe(
   data => {
    this.image = data.image; 
    this.country = data.country;
    this.lon = data.lon;
    this.lat = data.lat;  
      }, 
        error =>{
          console.log(error.message);
          this.hasError = true;
          this.errorMessage = error.message; 
           }
         )
       }
     }

