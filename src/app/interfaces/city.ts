export interface City {
    cityId : string;
    name : string;
    temperature: string;
    humidity: string;
}
