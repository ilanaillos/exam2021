// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{
    apiKey: "AIzaSyCX2BCZqBOVVGwsqDdbksKSkN4Mz4etht8",
    authDomain: "exam-58d03.firebaseapp.com",
    projectId: "exam-58d03",
    storageBucket: "exam-58d03.appspot.com",
    messagingSenderId: "498873242585",
    appId: "1:498873242585:web:912c98b471530e2f747fc8",
    measurementId: "G-JB33BZ1QHG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
